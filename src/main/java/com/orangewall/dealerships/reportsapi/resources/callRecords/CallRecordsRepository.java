package com.orangewall.dealerships.reportsapi.resources.callRecords;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Arjan on 3/05/2018.
 */
public interface CallRecordsRepository extends Repository<CallRecords, Integer> {

    @Query("SELECT new com.orangewall.dealerships.reportsapi.resources.callRecords.EmployeeEngagement(" +
            "sum(c.callQueueDuration), " +
            "c.userId, " +
            "count(c.contactId), " +
            "count (distinct c.customerEndpointAddress), " +
            "count(CASE WHEN c.connectedToAgentTimestamp IS NOT NULL THEN c.contactId ELSE null END), " +
            "avg(c.callQueueDuration), " +
            "count(CASE WHEN (c.connectedToIntendedAgent = 'true') THEN c.connectedToIntendedAgent ELSE null END)) " +
            "FROM CallRecords as c " +
            "where c.awsAccountId=?1 " +
            "AND c.connectedToSystemTimestamp>=?2 " +
            "AND c.connectedToSystemTimestamp<=?3 " +
            "GROUP BY c.userId")
    List<EmployeeEngagement> getEmployeeEngagement(String awsAccountId, LocalDateTime startDate, LocalDateTime endDate);

}
