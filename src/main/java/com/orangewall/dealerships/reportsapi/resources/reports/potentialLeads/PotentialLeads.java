package com.orangewall.dealerships.reportsapi.resources.reports.potentialLeads;

import com.orangewall.dealerships.reportsapi.common.jaxrsFormats.csv.CsvColumn;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;
import java.time.LocalDateTime;

/**
 * Created by Arjan on 7/11/2018.
 */

@XmlRootElement
@Entity
//@Table(name = "PotentialLeads")
public class PotentialLeads{
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    @Id
    @CsvColumn(name = "Date Interval")
    private LocalDateTime interval;
    private Long totalCalls;
    private Long connected;
    private Long unique;
    private Long leads;
    private Long appointmentConfirmed;
    private Long appointmentUnconfirmed;
    private Long appointmentRejected;
    private Long appointmentNotOffered;
    private Long agentUnavailable;
    private Long hangupWaiting;
    private Long leftMessage;
    private Long noMessageLeft;
    private Long noOneAvailable;


    public PotentialLeads() {
    }


    public PotentialLeads(LocalDateTime interval, Long totalCalls, Long connected, Long unique, Long leads, Long appointmentConfirmed, Long appointmentUnconfirmed, Long appointmentRejected, Long appointmentNotOffered, Long agentUnavailable, Long hangupWaiting, Long leftMessage, Long noMessageLeft, Long noOneAvailable) {
        this.interval = interval;
        this.totalCalls = totalCalls;
        this.connected = connected;
        this.unique = unique;
        this.leads = leads;
        this.appointmentConfirmed = appointmentConfirmed;
        this.appointmentUnconfirmed = appointmentUnconfirmed;
        this.appointmentRejected = appointmentRejected;
        this.appointmentNotOffered = appointmentNotOffered;
        this.agentUnavailable = agentUnavailable;
        this.hangupWaiting = hangupWaiting;
        this.leftMessage = leftMessage;
        this.noMessageLeft = noMessageLeft;
        this.noOneAvailable = noOneAvailable;
    }


    public Long getConnected() {
        return connected;
    }

    public void setConnected(Long connected) {
        this.connected = connected;
    }

    public Long getTotalCalls() {
        return totalCalls;
    }

    public void setTotalCalls(Long totalCalls) {
        this.totalCalls = totalCalls;
    }

    public Long getUnique() {
        return unique;
    }

    public void setUnique(Long unique) {
        this.unique = unique;
    }

    public Long getLeads() {
        return leads;
    }

    public void setLeads(Long leads) {
        this.leads = leads;
    }

    public Long getAppointmentConfirmed() {
        return appointmentConfirmed;
    }

    public void setAppointmentConfirmed(Long appointmentConfirmed) {
        this.appointmentConfirmed = appointmentConfirmed;
    }

    public Long getAppointmentUnconfirmed() {
        return appointmentUnconfirmed;
    }

    public void setAppointmentUnconfirmed(Long appointmentUnconfirmed) {
        this.appointmentUnconfirmed = appointmentUnconfirmed;
    }

    public Long getAppointmentRejected() {
        return appointmentRejected;
    }

    public void setAppointmentRejected(Long appointmentRejected) {
        this.appointmentRejected = appointmentRejected;
    }

    public Long getAppointmentNotOffered() {
        return appointmentNotOffered;
    }

    public void setAppointmentNotOffered(Long appointmentNotOffered) {
        this.appointmentNotOffered = appointmentNotOffered;
    }

    public LocalDateTime getInterval() {
        return interval;
    }

    public void setInterval(LocalDateTime interval) {
        this.interval = interval;
    }

    public Long getAgentUnavailable() {
        return agentUnavailable;
    }

    public void setAgentUnavailable(Long agentUnavailable) {
        this.agentUnavailable = agentUnavailable;
    }

    public Long getHangupWaiting() {
        return hangupWaiting;
    }

    public void setHangupWaiting(Long hangupWaiting) {
        this.hangupWaiting = hangupWaiting;
    }

    public Long getLeftMessage() {
        return leftMessage;
    }

    public void setLeftMessage(Long leftMessagee) {
        this.leftMessage = leftMessagee;
    }

    public Long getNoMessageLeft() {
        return noMessageLeft;
    }

    public void setNoMessageLeft(Long noMessageLeft) {
        this.noMessageLeft = noMessageLeft;
    }

    public Long getNoOneAvailable() {
        return noOneAvailable;
    }

    public void setNoOneAvailable(Long noOneAvailable) {
        this.noOneAvailable = noOneAvailable;
    }
}
