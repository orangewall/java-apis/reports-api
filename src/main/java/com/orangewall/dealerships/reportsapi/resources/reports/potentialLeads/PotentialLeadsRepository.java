package com.orangewall.dealerships.reportsapi.resources.reports.potentialLeads;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Arjan on 3/05/2018.
 */
public interface PotentialLeadsRepository extends Repository<PotentialLeads, Integer> {


    @Query(value = "SELECT date_trunc('hour', c.connected_to_system_timestamp) as interval,\n" +
            "            COUNT(c.contact_id) AS total_calls," +
            "            COUNT(CASE WHEN (c.connected_to_intended_agent = 'true') THEN c.contact_id ELSE null END) as connected, \n" +
            "            COUNT(distinct CASE WHEN c.connected_to_intended_agent = 'true' THEN (c.customer_endpoint_address) ELSE null END) as unique, \n" +
            "            COUNT(distinct case WHEN c.sales_lead = true THEN (c.customer_endpoint_address) ELSE null END) as leads, \n" +
            "            COUNT(distinct CASE WHEN c.sales_lead_feedback = 'Appointment Confirmed' THEN (c.customer_endpoint_address) ELSE NULL END) as appointment_confirmed, \n" +
            "            COUNT(distinct CASE WHEN c.sales_lead_feedback = 'Appointment Not Confirmed' THEN (c.customer_endpoint_address) ELSE NULL END) as appointment_unconfirmed, \n" +
            "            COUNT(distinct CASE WHEN c.sales_lead_feedback = 'Appointment Rejected' THEN (c.customer_endpoint_address) ELSE NULL END) as appointment_rejected, \n" +
            "            COUNT(distinct CASE WHEN c.sales_lead_feedback = 'Appointment Not Asked' THEN (c.customer_endpoint_address) ELSE NULL END) as appointment_not_offered,\n" +
            "            COUNT(distinct CASE WHEN c.connected_to_intended_agent = 'false' AND c.reason_not_connected = 'agent_unavailable' THEN c.customer_endpoint_address ELSE NULL END) AS agent_unavailable,\n" +
            "            COUNT(distinct CASE WHEN c.connected_to_intended_agent = 'false' AND c.reason_not_connected = 'hangup_waiting' THEN c.customer_endpoint_address ELSE NULL END) AS hangup_waiting,\n" +
            "            COUNT(distinct CASE WHEN c.connected_to_intended_agent = 'false' AND c.reason_not_connected = 'left_message' THEN c.customer_endpoint_address ELSE NULL END) AS left_message,\n" +
            "            COUNT(distinct CASE WHEN c.connected_to_intended_agent = 'false' AND c.reason_not_connected = 'no_message_left' THEN c.customer_endpoint_address ELSE NULL END) AS no_message_left,\n" +
            "            COUNT(distinct CASE WHEN c.connected_to_intended_agent = 'false' AND c.reason_not_connected = 'no_one_available' THEN c.customer_endpoint_address ELSE NULL END) AS no_one_available" +
            "       FROM call_records as c \n" +
            "       WHERE c.aws_account_id = :awsAccountId\n" +
            "            AND c.connected_to_system_timestamp>=:startDate\n" +
            "            AND c.connected_to_system_timestamp<=:endDate\n" +
            "            AND c.initiation_method in (:callTypeFilter)" +
            "       GROUP BY date_trunc('hour', c.connected_to_system_timestamp)" +
            "       ORDER BY date_trunc('hour', c.connected_to_system_timestamp) ASC", nativeQuery = true)
    List<PotentialLeads> getPotentialLeadsGroupByHour(@Param("awsAccountId") String awsAccountId, @Param("startDate") LocalDateTime startDate, @Param("endDate") LocalDateTime endDate, @Param("callTypeFilter") List<String> callTypeFilter);


    @Query(value = "SELECT date_trunc('day', c.connected_to_system_timestamp) as interval,\n" +
            "            COUNT(c.contact_id) AS total_calls," +
            "            COUNT(CASE WHEN (c.connected_to_intended_agent = 'true') THEN c.contact_id ELSE null END) as connected, \n" +
            "            COUNT(distinct CASE WHEN c.connected_to_intended_agent = 'true' THEN (c.customer_endpoint_address) ELSE null END) as unique, \n" +
            "            COUNT(distinct case WHEN c.sales_lead = true THEN (c.customer_endpoint_address) ELSE null END) as leads, \n" +
            "            COUNT(distinct CASE WHEN c.sales_lead_feedback = 'Appointment Confirmed' THEN (c.customer_endpoint_address) ELSE NULL END) as appointment_confirmed, \n" +
            "            COUNT(distinct CASE WHEN c.sales_lead_feedback = 'Appointment Not Confirmed' THEN (c.customer_endpoint_address) ELSE NULL END) as appointment_unconfirmed, \n" +
            "            COUNT(distinct CASE WHEN c.sales_lead_feedback = 'Appointment Rejected' THEN (c.customer_endpoint_address) ELSE NULL END) as appointment_rejected, \n" +
            "            COUNT(distinct CASE WHEN c.sales_lead_feedback = 'Appointment Not Asked' THEN (c.customer_endpoint_address) ELSE NULL END) as appointment_not_offered,\n" +
            "            COUNT(distinct CASE WHEN c.connected_to_intended_agent = 'false' AND c.reason_not_connected = 'agent_unavailable' THEN c.customer_endpoint_address ELSE NULL END) AS agent_unavailable,\n" +
            "            COUNT(distinct CASE WHEN c.connected_to_intended_agent = 'false' AND c.reason_not_connected = 'hangup_waiting' THEN c.customer_endpoint_address ELSE NULL END) AS hangup_waiting,\n" +
            "            COUNT(distinct CASE WHEN c.connected_to_intended_agent = 'false' AND c.reason_not_connected = 'left_message' THEN c.customer_endpoint_address ELSE NULL END) AS left_message,\n" +
            "            COUNT(distinct CASE WHEN c.connected_to_intended_agent = 'false' AND c.reason_not_connected = 'no_message_left' THEN c.customer_endpoint_address ELSE NULL END) AS no_message_left,\n" +
            "            COUNT(distinct CASE WHEN c.connected_to_intended_agent = 'false' AND c.reason_not_connected = 'no_one_available' THEN c.customer_endpoint_address ELSE NULL END) AS no_one_available" +
            "       FROM call_records as c \n" +
            "       WHERE c.aws_account_id = :awsAccountId\n" +
            "            AND c.connected_to_system_timestamp>=:startDate\n" +
            "            AND c.connected_to_system_timestamp<=:endDate\n" +
            "            AND c.initiation_method in (:callTypeFilter)" +
            "       GROUP BY date_trunc('day', c.connected_to_system_timestamp)" +
            "       ORDER BY date_trunc('day', c.connected_to_system_timestamp) ASC", nativeQuery = true)
    List<PotentialLeads> getPotentialLeadsGroupByDay(@Param("awsAccountId") String awsAccountId, @Param("startDate") LocalDateTime startDate, @Param("endDate") LocalDateTime endDate, @Param("callTypeFilter") List<String> callTypeFilter);
}
