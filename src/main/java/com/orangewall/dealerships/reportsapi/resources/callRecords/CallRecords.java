package com.orangewall.dealerships.reportsapi.resources.callRecords;

import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Created by Arjan on 3/05/2018.
 */

//@XmlRootElement
@Entity
@Table(name = "CallRecords")
public class CallRecords {

    @Id
    @GeneratedValue
    private Integer id;
    private String contactId;
    private String userId;
    @NotNull
    private String awsAccountId;
    private Double afterContactWorkDuration;
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime afterContactWorkStartTimestamp;
    private Integer agentInteractionDuration;
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime connectedToAgentTimestamp;
    private String customerEndpointAddress;
    private String systemEndpointAddress;
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime connectedToSystemTimestamp;
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime disconnectTimestamp;
    private Double callQueueDuration;
    private String initiationMethod;
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    private LocalDateTime lastUpdateTimestamp;
    private String recordingLocation;
    private Integer customerHoldDuration;
    private String connectedToIntendedAgent;
    private String connectedWithin;
    private String reasonNotConnected;
    private Boolean salesLead;
    private String salesLeadFeedback;

    public CallRecords() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAwsAccountId() {
        return awsAccountId;
    }

    public void setAwsAccountId(String awsAccountId) {
        this.awsAccountId = awsAccountId;
    }

    public Double getAfterContactWorkDuration() {
        return afterContactWorkDuration;
    }

    public void setAfterContactWorkDuration(Double afterContactWorkDuration) {
        this.afterContactWorkDuration = afterContactWorkDuration;
    }

    public LocalDateTime getAfterContactWorkStartTimestamp() {
        return afterContactWorkStartTimestamp;
    }

    public void setAfterContactWorkStartTimestamp(LocalDateTime afterContactWorkStartTimestamp) {
        this.afterContactWorkStartTimestamp = afterContactWorkStartTimestamp;
    }

    public Integer getAgentInteractionDuration() {
        return agentInteractionDuration;
    }

    public void setAgentInteractionDuration(Integer agentInteractionDuration) {
        this.agentInteractionDuration = agentInteractionDuration;
    }

    public LocalDateTime getConnectedToAgentTimestamp() {
        return connectedToAgentTimestamp;
    }

    public void setConnectedToAgentTimestamp(LocalDateTime connectedToAgentTimestamp) {
        this.connectedToAgentTimestamp = connectedToAgentTimestamp;
    }

    public String getCustomerEndpointAddress() {
        return customerEndpointAddress;
    }

    public void setCustomerEndpointAddress(String customerEndpointAddress) {
        this.customerEndpointAddress = customerEndpointAddress;
    }

    public String getSystemEndpointAddress() {
        return systemEndpointAddress;
    }

    public void setSystemEndpointAddress(String systemEndpointAddress) {
        this.systemEndpointAddress = systemEndpointAddress;
    }

    public LocalDateTime getConnectedToSystemTimestamp() {
        return connectedToSystemTimestamp;
    }

    public void setConnectedToSystemTimestamp(LocalDateTime connectedToSystemTimestamp) {
        this.connectedToSystemTimestamp = connectedToSystemTimestamp;
    }

    public LocalDateTime getDisconnectTimestamp() {
        return disconnectTimestamp;
    }

    public void setDisconnectTimestamp(LocalDateTime disconnectTimestamp) {
        this.disconnectTimestamp = disconnectTimestamp;
    }

    public Double getCallQueueDuration() {
        return callQueueDuration;
    }

    public void setCallQueueDuration(Double callQueueDuration) {
        this.callQueueDuration = callQueueDuration;
    }

    public String getInitiationMethod() {
        return initiationMethod;
    }

    public void setInitiationMethod(String initiationMethod) {
        this.initiationMethod = initiationMethod;
    }

    public LocalDateTime getLastUpdateTimestamp() {
        return lastUpdateTimestamp;
    }

    public void setLastUpdateTimestamp(LocalDateTime lastUpdateTimestamp) {
        this.lastUpdateTimestamp = lastUpdateTimestamp;
    }

    public String getRecordingLocation() {
        return recordingLocation;
    }

    public void setRecordingLocation(String recordingLocation) {
        this.recordingLocation = recordingLocation;
    }

    public Integer getCustomerHoldDuration() {
        return customerHoldDuration;
    }

    public void setCustomerHoldDuration(Integer customerHoldDuration) {
        this.customerHoldDuration = customerHoldDuration;
    }

    public String getConnectedToIntendedAgent() {
        return connectedToIntendedAgent;
    }

    public void setConnectedToIntendedAgent(String connectedToIntendedAgent) {
        this.connectedToIntendedAgent = connectedToIntendedAgent;
    }

    public String getConnectedWithin() {
        return connectedWithin;
    }

    public void setConnectedWithin(String connectedWithin) {
        this.connectedWithin = connectedWithin;
    }

    public String getReasonNotConnected() {
        return reasonNotConnected;
    }

    public void setReasonNotConnected(String reasonNotConnected) {
        this.reasonNotConnected = reasonNotConnected;
    }

    public Boolean getSalesLead() {
        return salesLead;
    }

    public void setSalesLead(Boolean salesLead) {
        this.salesLead = salesLead;
    }

    public String getSalesLeadFeedback() {
        return salesLeadFeedback;
    }

    public void setSalesLeadFeedback(String salesLeadFeedback) {
        this.salesLeadFeedback = salesLeadFeedback;
    }
}
