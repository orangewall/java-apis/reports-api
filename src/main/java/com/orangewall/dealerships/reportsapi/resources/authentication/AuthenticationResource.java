package com.orangewall.dealerships.reportsapi.resources.authentication;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.orangewall.dealerships.reportsapi.resources.authentication.model.AuthenticationUser;
import com.orangewall.dealerships.reportsapi.resources.authentication.model.Credentials;
import com.orangewall.dealerships.reportsapi.resources.authentication.repositories.AuthenticationUserRepository;
import com.orangewall.dealerships.reportsapi.resources.authentication.repositories.GeneratedTokenRepository;
import com.orangewall.dealerships.reportsapi.resources.user.PasswordUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * Created by kbw780 on 6/5/17.
 */
@Path("authentication")
@Produces(MediaType.APPLICATION_JSON)
@Component
public class AuthenticationResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationResource.class);

    @Value("${authentication.token.timeout:3600}")
    private int authenticationTokenTimeout;

    @Value("${authentication.token.encryption.key}")
    private String authenticationTokenEncryptionKey;

    @Autowired
    AuthenticationUserRepository userRepository;

    @Autowired
    GeneratedTokenRepository generatedTokenRepository;


    @Context //This annotation is used to inject objects from context. Objects like UserInfo, Request etc
    private UriInfo uriInfo;

    @Path("/authenticate")
    @POST
    @PermitAll
    @Produces(MediaType.TEXT_PLAIN)
    public Response authenticateJwt(Credentials credentials) {
        try {
            AuthenticationUser user = userRepository.getByUsername(credentials.getUsername());

            if (user == null || !user.isActive() || !PasswordUtil.check(credentials.getPassword(), user.getPassword())) {
                //TODO: Check if authentication should return UNAUTHORIZED or just return code 200 with body as null.
                return Response.status(Response.Status.UNAUTHORIZED).build();
            } else {
                //TODO: Check which Algorithm is the best option. This should be good as well. https://github.com/auth0/java-jwt
                Algorithm algorithm = Algorithm.HMAC256(authenticationTokenEncryptionKey);

                LocalDateTime ldt = LocalDateTime.now().plusHours(1);
                Date expirationDate = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());

                String token = JWT.create()
                        .withExpiresAt(expirationDate)
                        .withClaim("role",user.getRole())
                        .withIssuer("ot")
                        .sign(algorithm);


                LOGGER.info("User: '{}' authenticated successfully!", user.getUsername());
                return Response.ok(token).build();
            }
        } catch (IllegalStateException e) {
            LOGGER.error("Unexpected error happened on authenticating the user!", e);
            return Response.status(Response.Status.UNAUTHORIZED).build();
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("Error when encoding the token!", e);
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }


    @Path("/validate-token")
    @POST
    @PermitAll
    public Response validateJwt(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(authenticationTokenEncryptionKey);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer("ot")
                    .build(); //Reusable verifier instance
            DecodedJWT jwt = verifier.verify(token);
            Claim roleClaim = jwt.getClaim("role");
            String role = null;
            if(roleClaim != null){
                role = roleClaim.asString();
            }
            return Response.ok(role).build();
        } catch (UnsupportedEncodingException e) {
            //UTF-8 encoding not supported
            LOGGER.error("Error when verifying the token!", e);
            return Response.status(Response.Status.UNAUTHORIZED).build();
        } catch (JWTVerificationException e) {
            //Invalid signature/claims
            LOGGER.error("Token is not valid!", e);
            return Response.ok(null).build();
        }
    }

}
