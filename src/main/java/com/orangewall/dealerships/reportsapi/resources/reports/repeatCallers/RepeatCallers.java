package com.orangewall.dealerships.reportsapi.resources.reports.repeatCallers;

import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Arjan on 8/23/2018.
 */

@XmlRootElement
@Entity
public class RepeatCallers {
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    @Id
    private Integer number_of_times_called;
    private Integer number_of_callers;

    public RepeatCallers() {
    }


    public RepeatCallers(Integer number_of_times_called, Integer number_of_callers) {
        this.number_of_times_called = number_of_times_called;
        this.number_of_callers = number_of_callers;
    }



    public Integer getNumber_of_times_called() {
        return number_of_times_called;
    }

    public void setNumber_of_times_called(Integer number_of_times_called) {
        this.number_of_times_called = number_of_times_called;
    }

    public Integer getNumber_of_callers() {
        return number_of_callers;
    }

    public void setNumber_of_callers(Integer number_of_callers) {
        this.number_of_callers = number_of_callers;
    }

}
