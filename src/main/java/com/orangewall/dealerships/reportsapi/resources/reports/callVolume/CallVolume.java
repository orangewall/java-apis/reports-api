package com.orangewall.dealerships.reportsapi.resources.reports.callVolume;

import com.orangewall.dealerships.reportsapi.common.jaxrsFormats.csv.CsvColumn;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;
import java.time.LocalDateTime;

/**
 * Created by Arjan on 7/11/2018.
 */

@XmlRootElement
@Entity
public class CallVolume {
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    @Id
    @CsvColumn(name = "Date Interval")
    private LocalDateTime interval;
    private Long totalCalls;
    private Integer totalMinutes;
    private Double avgCallDuration;
    private Long droppedCalls;
    private Double avgHold;
    private Integer holdLessThanTarget;
    private Integer holdMoreThanDoubleTarget;
    private Integer hold_less_than_30_sec;
    private Integer hold_31_to_60_sec;
    private Integer hold_61_to_120;
    private Integer hold_more_than_120_sec;

    public CallVolume() {
    }


    public CallVolume(
                        LocalDateTime interval,
                        Long totalCalls,
                        Double avgCallDuration,
                        Long droppedCalls,
                        Double avgHold,
                        Integer holdLessThanTarget,
                        Integer holdMoreThanDoubleTarget,
                        Integer hold_less_than_30_sec,
                        Integer hold_31_to_60_sec,
                        Integer hold_61_to_120,
                        Integer hold_more_than_120_sec)
    {
        this.interval = interval;
        this.totalCalls = totalCalls;
        this.avgCallDuration = avgCallDuration;
        this.droppedCalls = droppedCalls;
        this.avgHold = avgHold;
        this.holdLessThanTarget = holdLessThanTarget;
        this.holdMoreThanDoubleTarget = holdMoreThanDoubleTarget;
        this.hold_less_than_30_sec = hold_less_than_30_sec;
        this.hold_31_to_60_sec = hold_31_to_60_sec;
        this.hold_61_to_120 = hold_61_to_120;
        this.hold_more_than_120_sec = hold_more_than_120_sec;
    }

    public LocalDateTime getInterval() {
        return interval;
    }

    public void setInterval(LocalDateTime interval) {
        this.interval = interval;
    }

    public Long getTotalCalls() {
        return totalCalls;
    }

    public void setTotalCalls(Long totalCalls) {
        this.totalCalls = totalCalls;
    }

    public Integer getTotalMinutes() {
        return totalMinutes;
    }

    public void setTotalMinutes(Integer totalMinutes) {
        this.totalMinutes = totalMinutes;
    }

    public Double getAvgCallDuration() {
        return avgCallDuration;
    }

    public void setAvgCallDuration(Double avgCallDuration) {
        this.avgCallDuration = avgCallDuration;
    }

    public Long getDroppedCalls() {
        return droppedCalls;
    }

    public void setDroppedCalls(Long droppedCalls) {
        this.droppedCalls = droppedCalls;
    }

    public Double getAvgHold() {
        return avgHold;
    }

    public void setAvgHold(Double avgHold) {
        this.avgHold = avgHold;
    }

    public Integer getHoldLessThanTarget() {
        return holdLessThanTarget;
    }

    public void setHoldLessThanTarget(Integer holdLessThanTarget) {
        this.holdLessThanTarget = holdLessThanTarget;
    }

    public Integer getHoldMoreThanDoubleTarget() {
        return holdMoreThanDoubleTarget;
    }

    public void setHoldMoreThanDoubleTarget(Integer holdMoreThanDoubleTarget) {
        this.holdMoreThanDoubleTarget = holdMoreThanDoubleTarget;
    }


    public Integer getHold_less_than_30_sec() {
        return hold_less_than_30_sec;
    }

    public void setHold_less_than_30_sec(Integer hold_less_than_30_sec) {
        this.hold_less_than_30_sec = hold_less_than_30_sec;
    }

    public Integer getHold_31_to_60_sec() {
        return hold_31_to_60_sec;
    }

    public void setHold_31_to_60_sec(Integer hold_31_to_60_sec) {
        this.hold_31_to_60_sec = hold_31_to_60_sec;
    }

    public Integer getHold_61_to_120() {
        return hold_61_to_120;
    }

    public void setHold_61_to_120(Integer hold_61_to_120) {
        this.hold_61_to_120 = hold_61_to_120;
    }

    public Integer getHold_more_than_120_sec() {
        return hold_more_than_120_sec;
    }

    public void setHold_more_than_120_sec(Integer hold_more_than_120_sec) {
        this.hold_more_than_120_sec = hold_more_than_120_sec;
    }

}
