package com.orangewall.dealerships.reportsapi.resources.authentication.repositories;


import com.orangewall.dealerships.reportsapi.resources.authentication.model.GeneratedToken;
import org.springframework.data.repository.CrudRepository;

public interface GeneratedTokenRepository extends CrudRepository<GeneratedToken, String> {

}
