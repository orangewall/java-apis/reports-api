package com.orangewall.dealerships.reportsapi.resources.reports.callVolume;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Arjan on 3/05/2018.
 */
public interface CallVolumeRepository extends Repository<CallVolume, Integer> {


    @Query(value = "SELECT date_trunc('hour', c.connected_to_system_timestamp) as interval,\n" +
            "            COUNT(c.contact_id) AS total_calls," +
            "            SUM(c.call_queue_duration) as total_minutes," +
            "            AVG(call_queue_duration) AS avg_call_duration," +
            "            COUNT(CASE WHEN (c.user_id is null) THEN c.contact_id ELSE null END) as dropped_calls," +
            "            AVG(customer_hold_duration) AS avg_hold," +
            "            COUNT(CASE WHEN (c.customer_hold_duration <= 30) THEN c.contact_id ELSE null END) as hold_less_than_target," +
            "            COUNT(CASE WHEN (c.customer_hold_duration >= 60) THEN c.contact_id ELSE null END) as hold_more_than_double_target," +
            "            COUNT(CASE WHEN (c.customer_hold_duration <= 30) THEN c.contact_id ELSE null END) as hold_less_than_30_sec,\n" +
            "            COUNT(CASE WHEN (c.customer_hold_duration > 30 AND c.customer_hold_duration <= 60) THEN c.contact_id ELSE null END) as hold_31_to_60_sec,\n" +
            "            COUNT(CASE WHEN (c.customer_hold_duration > 60 AND c.customer_hold_duration <= 120) THEN c.contact_id ELSE null END) as hold_61_to_120,\n" +
            "            COUNT(CASE WHEN (c.customer_hold_duration > 120) THEN c.contact_id ELSE null END) as hold_more_than_120_sec \n" +
            "       FROM call_records as c \n" +
            "       WHERE c.aws_account_id = :awsAccountId\n" +
            "            AND c.connected_to_system_timestamp>=:startDate\n" +
            "            AND c.connected_to_system_timestamp<=:endDate\n" +
            "            AND c.initiation_method in (:callTypeFilter)" +
            "       GROUP BY date_trunc('hour', c.connected_to_system_timestamp)" +
            "       ORDER BY date_trunc('hour', c.connected_to_system_timestamp) ASC", nativeQuery = true)
    List<CallVolume> getCallVolumeGroupByHour(@Param("awsAccountId") String awsAccountId, @Param("startDate") LocalDateTime startDate, @Param("endDate") LocalDateTime endDate, @Param("callTypeFilter") List<String> callTypeFilter);


    @Query(value = "SELECT date_trunc('day', c.connected_to_system_timestamp) as interval,\n" +
            "            COUNT(c.contact_id) AS total_calls," +
            "            SUM(c.call_queue_duration) as total_minutes," +
            "            AVG(call_queue_duration) AS avg_call_duration," +
            "            COUNT(CASE WHEN (c.user_id is null) THEN c.contact_id ELSE null END) as dropped_calls," +
            "            AVG(customer_hold_duration) AS avg_hold," +
            "            COUNT(CASE WHEN (c.customer_hold_duration <= 30) THEN c.contact_id ELSE null END) as hold_less_than_target," +
            "            COUNT(CASE WHEN (c.customer_hold_duration >= 60) THEN c.contact_id ELSE null END) as hold_more_than_double_target," +
            "            COUNT(CASE WHEN (c.customer_hold_duration <= 30) THEN c.contact_id ELSE null END) as hold_less_than_30_sec,\n" +
            "            COUNT(CASE WHEN (c.customer_hold_duration > 30 AND c.customer_hold_duration <= 60) THEN c.contact_id ELSE null END) as hold_31_to_60_sec,\n" +
            "            COUNT(CASE WHEN (c.customer_hold_duration > 60 AND c.customer_hold_duration <= 120) THEN c.contact_id ELSE null END) as hold_61_to_120,\n" +
            "            COUNT(CASE WHEN (c.customer_hold_duration > 120) THEN c.contact_id ELSE null END) as hold_more_than_120_sec \n" +
            "       FROM call_records as c \n" +
            "       WHERE c.aws_account_id = :awsAccountId\n" +
            "            AND c.connected_to_system_timestamp>=:startDate\n" +
            "            AND c.connected_to_system_timestamp<=:endDate\n" +
            "            AND c.initiation_method in (:callTypeFilter)" +
            "       GROUP BY date_trunc('day', c.connected_to_system_timestamp)" +
            "       ORDER BY date_trunc('day', c.connected_to_system_timestamp) ASC", nativeQuery = true)
    List<CallVolume> getCallVolumeGroupByDay(@Param("awsAccountId") String awsAccountId, @Param("startDate") LocalDateTime startDate, @Param("endDate") LocalDateTime endDate, @Param("callTypeFilter") List<String> callTypeFilter);
}
