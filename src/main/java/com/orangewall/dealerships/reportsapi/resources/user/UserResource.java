package com.orangewall.dealerships.reportsapi.resources.user;

import org.glassfish.jersey.internal.guava.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.List;

/**
 * Created by kbw780 on 6/4/17.
 */

@Path("users")
@Produces(MediaType.APPLICATION_JSON)
@Component
public class UserResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserResource.class);

    @Autowired
    UserRepository userRepository;

    @Context //This annotation is used to inject objects from context. Objects like UserInfo, Request etc
    private UriInfo uriInfo;


    @GET
    @RolesAllowed("admin")
//    @PermitAll
    public Response getAll() {
        final List<User> users = Lists.newArrayList(userRepository.findAll());

        return Response.ok(users).build();
    }

    @GET
    @PermitAll
    @Path("/{userId}")
    public Response getById(@PathParam("userId") final Long userId) {
        final User user;
        try {
            user = userRepository.findById(userId).get();
        } catch (Exception e) {
            LOGGER.error(String.format("Unexpected error happened while getting user with ID: '%s'!", userId), e);
            return Response.serverError().build();
        }
        if (user == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(user).build();
    }

    @PUT
    @PermitAll
    @Path("/{userId}") //Untested yet
    public Response save(@PathParam("userId") final Long userId, User user) {
        User currentUser = userRepository.findById(userId).get();

        if (currentUser == null) {
            LOGGER.info("Updating user with ID: '{}' failed since that user doesn't exist.");
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        try {
            user = userRepository.save(user);
        } catch (Exception e) {
            LOGGER.error("Unexpected error happened when updating user!", e);
            return Response.serverError().build();
        }
        LOGGER.info("User with id: '{}' got updated successfully!", userId);
        return Response.ok(user).build();
    }

    @POST
    @PermitAll
    public Response create(User user) {
        try {
            User existingUser = userRepository.getByUsername(user.getUsername());
            if(existingUser != null){
                return Response.status(Response.Status.CONFLICT)
                        .entity("User with username '"+user.getUsername()+"' exists!")
                        .build();
            }

            user.setPassword(PasswordUtil.getSaltedHash(user.getPassword()));

            userRepository.save(user);

            URI location = uriInfo.getAbsolutePathBuilder()
                    .path("{id}")
                    .resolveTemplate("id", user.getId())
                    .build();

            LOGGER.info("User with id: '{}' got created successfully!", user.getId());
            return Response.created(location).build();
        } catch (Exception e) {
            LOGGER.error("Unexpected error happened while creating a new user!", e);
            return Response.serverError().build();
        }
    }

    @DELETE
    @RolesAllowed("admin")
    @Path("{userId}")
    public Response delete(@PathParam("userId") Long userId) {
        userRepository.deleteById(userId);
        return Response.accepted().build();
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    //@PermitAll
    @Path("/health")
    public Response healthCheck() {
        return Response.ok("users health check - OK").build();
    }
}
