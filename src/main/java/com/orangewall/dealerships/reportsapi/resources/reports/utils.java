package com.orangewall.dealerships.reportsapi.resources.reports;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arjan on 8/13/2018.
 */
public class utils {

  public static List<String> setCallTypeFilter(String callType) {
      List<String> callTypeFilter = new ArrayList<>();
      if(callType.toUpperCase().equals("ALL")) {
          callTypeFilter.add("INBOUND");
          callTypeFilter.add("OUTBOUND");
      }
      else if(callType.toUpperCase().equals("INBOUND")) {
          callTypeFilter.add("INBOUND");
      }
      else if(callType.toUpperCase().equals("OUTBOUND")) {
          callTypeFilter.add("OUTBOUND");
      }
      else {
          callTypeFilter.add("invalid");
      }

      return callTypeFilter;
  }
}
