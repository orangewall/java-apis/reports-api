package com.orangewall.dealerships.reportsapi.resources.reports.repeatCallers;

import com.orangewall.dealerships.reportsapi.resources.reports.utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Arjan on 3/05/2018.
 */

@Path("reports")
@Component
public class RepeatCallersResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(RepeatCallersResource.class);

    @Autowired
    RepeatCallersRepository repeatCallersRepository;

    @Context
    UriInfo uriInfo;

    @Context
    Request request;

    @Context
    HttpServletRequest requestContext;

    private String awsAccountId="12";

    @GET
    @PermitAll
    @Path("repeatCallers")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, "text/csv"})
    public Response getCallVolume(@QueryParam("startDate") String startDate, @QueryParam("endDate") String endDate, @QueryParam("callType") String callType) {


        if (startDate == null || startDate.isEmpty() || endDate == null || endDate.isEmpty() || callType == null || callType.isEmpty()) {
            LOGGER.error("API was called with the following parameters: startDate {}, endDate {}, callType {}", startDate, endDate, callType);
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        //Defines if calls need to be filter by INBOUND, OUTBOUND or return all.
        List<String> callTypeFilter;

        callTypeFilter = utils.setCallTypeFilter(callType);
        if(callTypeFilter.contains("invalid")) {
            LOGGER.error("Invalid parameters are passed: startDate {}, endDate {}, callType {}", startDate, endDate, callType);
            return Response.status(Response.Status.BAD_REQUEST).build();
        }


        if (startDate.length() == 10) {
            startDate = startDate + " 00:00";
        }
        if (endDate.length() == 10) {
            endDate = endDate + " 23:59";
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime start = LocalDateTime.parse(startDate, formatter);
        LocalDateTime end = LocalDateTime.parse(endDate, formatter);

        List<RepeatCallers> listOfLead = null;

        try {
                listOfLead = repeatCallersRepository.getRepeatCallers(awsAccountId, start, end, callTypeFilter);
                listOfLead = addDaysWithNoDataList(start, end, listOfLead);
        }
         catch (Exception ex) {
            LOGGER.error("Unexpected error when pulling the RepeatCallers object");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        LOGGER.info("API has successfully return this object {}", listOfLead);
        return Response.ok(listOfLead).build();
    }


    private List<RepeatCallers> addDaysWithNoDataList(LocalDateTime startDate, LocalDateTime endDate, List<RepeatCallers> list) {

        List<RepeatCallers> listOfLeadsIncludeNoDataObjects = new ArrayList<RepeatCallers>();
        Integer counter = 0;

        if(list.isEmpty() || list == null) {
            for (int i=0; i<4; i++) {
                RepeatCallers emptyObject = new RepeatCallers(i+1,0);
                listOfLeadsIncludeNoDataObjects.add(emptyObject);
            }
        }
        else {
            //The list of objects has to be at least of size = 4.
            Integer loopSize;
            if(list.get(list.size() - 1).getNumber_of_times_called() > 4) {
                loopSize = list.get(list.size() - 1).getNumber_of_times_called();
            }
            else {
                loopSize = 4;
            }
            for (int i=0; i<loopSize; i++) {

                if (counter >= list.size()) {
                    RepeatCallers emptyObject = new RepeatCallers(i+1,0);
                    listOfLeadsIncludeNoDataObjects.add(emptyObject);
                } else {
                    RepeatCallers object = list.get(counter);
                    if(object.getNumber_of_times_called() == i+1) {
                        listOfLeadsIncludeNoDataObjects.add(object);
                        counter++;
                    }
                    else {
                        RepeatCallers emptyObject = new RepeatCallers(i+1,0);
                        listOfLeadsIncludeNoDataObjects.add(emptyObject);
                    }
                }

            }
        }

        return listOfLeadsIncludeNoDataObjects;
    }


}
