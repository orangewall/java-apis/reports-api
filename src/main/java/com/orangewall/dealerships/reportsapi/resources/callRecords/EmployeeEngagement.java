package com.orangewall.dealerships.reportsapi.resources.callRecords;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Arjan on 3/7/2018.
 */
@XmlRootElement
public class EmployeeEngagement {
    @Id
    @GeneratedValue
    private Integer id;
    private Double totalMinutes;
    private String userId; //agent
    private Long totalConversations;
    private Long uniqueConversations;
    private Long liveConversation;
    private Double averageTalkTime;
    private Long connected;
//    private Double averageAfterCallDuration;


    public EmployeeEngagement(String userId, Long totalConversations) {
        this.userId = userId;
        this.totalConversations = totalConversations;
    }


    public EmployeeEngagement(Double totalMinutes, String userId, Long totalConversations, Long uniqueConversations, Long liveConversation, Double averageTalkTime, Long connected) {
        this.totalMinutes = totalMinutes;
        this.userId = userId;
        this.totalConversations = totalConversations;
        this.uniqueConversations = uniqueConversations;
        this.liveConversation = liveConversation;
        this.averageTalkTime = averageTalkTime;
        this.connected = connected;
    }


    public String getStaffId() {
        return userId;
    }

    public void setStaffId(String staffId) {
        this.userId = staffId;
    }

    public Long getTotalConversations() {
        return totalConversations;
    }

    public void setTotalConversations(Long totalConversations) {
        this.totalConversations = totalConversations;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getUniqueConversations() {
        return uniqueConversations;
    }

    public void setUniqueConversations(Long uniqueConversations) {
        this.uniqueConversations = uniqueConversations;
    }

    public Long getLiveConversation() {
        return liveConversation;
    }

    public void setLiveConversation(Long liveConversation) {
        this.liveConversation = liveConversation;
    }

    public Double getAverageTalkTime() {
        return averageTalkTime;
    }

    public void setAverageTalkTime(Double averageTalkTime) {
        this.averageTalkTime = averageTalkTime;
    }

    public Double getTotalMinutes() {
        return totalMinutes;
    }

    public void setTotalMinutes(Double totalMinutes) {
        this.totalMinutes = totalMinutes;
    }

    public Long getConnected() {
        return connected;
    }

    public void setConnected(Long connected) {
        this.connected = connected;
    }

}
