package com.orangewall.dealerships.reportsapi.resources.employee;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Created by Arjan on 11/26/2017.
 */
public interface EmployeeRepository extends PagingAndSortingRepository<Employee, Integer> {
    List<Employee> findByFirstName(String firstName);
    List<Employee> findByFirstNameAndLastName(String firstName, String lastName);
    List<Employee> findByLastName(String lastName);
    List<Employee> findByFedexId(String fedexId);
    List<Employee> findByContractorId(Integer contractorId);
}
