package com.orangewall.dealerships.reportsapi.resources.employee;//package com.onlinetrucking.resources.employee;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.security.PermitAll;
//import javax.servlet.http.HttpServletRequest;
//import javax.ws.rs.*;
//import javax.ws.rs.core.*;
//import java.net.URI;
//import java.time.LocalDateTime;
//import java.util.ArrayList;
//import java.util.List;
//
//
//
//
///**
// * Created by Arjan on 11/10/2017.
// */
//
//@Path("employees")
//@Component
//public class EmployeeResource {
//
//    @Autowired
//    EmployeeRepository employeeRepository;
//
//    @Context
//    UriInfo uriInfo;
//    @Context
//    Request request;
//    @Context
//    HttpServletRequest requestContext;
//
//    @GET
//    @PermitAll
//    @Path("/{id}")
//    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    public Response getEmployee(@PathParam("id") Integer id) {
//
//        try {
//            Employee empToReturn = employeeRepository.findOne(id);
//            if (empToReturn == null) {
//                return Response.status(Response.Status.NOT_FOUND).build();
//            } else {
//                return Response.ok(empToReturn).build();
//            }
//        }
//        catch (Exception ex) {
//            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
//        }
//
//    }
//
//
//    @GET
//    @PermitAll
//    @QueryParam("{firstName}, {lastName}, {fedexId}, {contractorId}")
//    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    public Response getEmployees(@QueryParam("firstName") String firstName, @QueryParam("lastName") String lastName, @QueryParam("fedexId") String fedexId, @QueryParam("contractorId") Integer contractorId) {
//
//     try {
//            List<Employee> employeeList;
//
//             if(firstName != null && !firstName.isEmpty() && lastName != null && !lastName.isEmpty()) {
//
//                 employeeList = employeeRepository.findByFirstNameAndLastName(firstName, lastName);
//             }
//             else if(firstName != null && !firstName.isEmpty()) {
//                 employeeList = employeeRepository.findByFirstName(firstName);
//             }
//             else if (lastName != null && !lastName.isEmpty()) {
//                 employeeList = employeeRepository.findByLastName(lastName);
//             }
//             else if (fedexId != null && !fedexId.isEmpty()) {
//                 employeeList = employeeRepository.findByFedexId(fedexId);
//             }
//             else if(contractorId != null) {
//                 employeeList = employeeRepository.findByContractorId(contractorId);
//             }
//             else {
//                 employeeList = (List<Employee>) employeeRepository.findAll();
//             }
//
//             if (employeeList.size() == 0) {
//                 return Response.status(Response.Status.NOT_FOUND).build();
//             } else {
//                 return Response.ok(employeeList).build();
//             }
//
//     }
//     catch (Exception ex) {
//         return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
//     }
//
//    }
//
//
//    @PUT
//    @PermitAll
//    @Path("/{id}")
//    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    public Response putEmployee(@PathParam("id") Integer id, Employee employee) {
//
//    if(validateRequiredFieldsEmployee(employee)) {
//        try {
//            Employee employeeToModify = employeeRepository.findOne(id);
//            employeeToModify.setFirstName(employee.getFirstName());
//            employeeToModify.setLastName(employee.getLastName());
//            employeeToModify.setFedexId(employee.getFedexId());
////            employeeToModify.setStarDate(employee.getStarDate());
////            employeeToModify.setEndDate(employee.getEndDate());
////            employeeToModify.setPayRate(employee.getPayRate());
////            employeeToModify.setActive(employee.isActive());
//            employeeToModify.setPhone(employee.getPhone());
////            employeeToModify.setEmail(employee.getEmail());
////            employeeToModify.setNote(employee.getNote());
//            employee.setModifiedIP(requestContext.getRemoteAddr());
//            employee.setModifiedbyId(1); //Implement logic to get the userid
//            employee.setModifiedOn(LocalDateTime.now());
//            employeeRepository.save(employeeToModify);
//            return Response.ok(employeeToModify).build();
//        }
//        catch(Exception ex) {
//            return Response.status(Response.Status.BAD_REQUEST).build();
//        }
//    }
//    else {
//        return Response.status(Response.Status.BAD_REQUEST).build();
//    }
// }
//
//    @POST
//    @PermitAll
//    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
//    public Response postEmployee(Employee employee) {
//
//    if(validateRequiredFieldsEmployee(employee)) {
//        try {
//            employee.setCreatedIP(requestContext.getRemoteAddr());
//            employee.setCreatedbyId(1); //Implement logic to get UserId
//            employee.setCreatedOn(LocalDateTime.now());
//            employeeRepository.save(employee);
//        }
//        catch(Exception ex) {
//
//        }
//
//        URI location = uriInfo.getAbsolutePathBuilder()
//                .path("{id}")
//                .resolveTemplate("id", employee.getId())
//                .build();
//        return Response.created(location).build();
//    }
//    else {
//        return Response.status(Response.Status.BAD_REQUEST).build();
//    }
//
// }
//
//    @DELETE
//    @PermitAll
//    @Path("/{id}")
//    public Response deleteEmployee(@PathParam("id") Integer id) {
//        Response status = Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
//        try {
//            Employee empToDelete = employeeRepository.findOne(id);
//            if (empToDelete == null) {
//                status = Response.status(Response.Status.NOT_FOUND).build();
//            } else {
//                employeeRepository.delete(id);
//                status = Response.status(Response.Status.ACCEPTED).build(); //Can also consider 204 (no content) or 200 (OK)
//            }
//        }
//        catch(Exception ex){
//
//            }
//
//       return status;
//    }
//
//    @GET
//    @PermitAll
//    @Produces({MediaType.TEXT_PLAIN})
//    @Path("/health")
//    public String checkHeath() {
//        return "EmployeeResource service is healthy!";
//    }
//
//
//    private boolean validateRequiredFieldsEmployee(Employee employeeToValidate) {
//        if(employeeToValidate.getFirstName() == null || employeeToValidate.getFirstName().isEmpty()) return false;
//        else if(employeeToValidate.getLastName() == null || employeeToValidate.getLastName().isEmpty()) return false;
//        else if(employeeToValidate.getFedexId() == null || employeeToValidate.getFedexId().isEmpty()) return false;
//        else if (employeeToValidate.getContractorId() == null || employeeToValidate.getContractorId() == 0) return false;
//    else return true;
//    }
//
//}
