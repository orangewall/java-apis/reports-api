package com.orangewall.dealerships.reportsapi.resources.reports.potentialLeads;

import com.orangewall.dealerships.reportsapi.resources.reports.utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Arjan on 3/05/2018.
 */

@Path("reports")
@Component
public class PotentialLeadsResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(PotentialLeadsResource.class);

    @Autowired
    PotentialLeadsRepository potentialLeadsRepository;

    @Context
    UriInfo uriInfo;

    @Context
    Request request;

    @Context
    HttpServletRequest requestContext;

    private String awsAccountId="12";

    @GET
    @PermitAll
    @Path("potentialLeads")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, "text/csv"})
    public Response getPotentialLeads(@QueryParam("startDate") String startDate, @QueryParam("endDate") String endDate, @QueryParam("groupBy") String groupBy, @QueryParam("callType") String callType) {

        if (startDate == null || startDate.isEmpty() || endDate == null || endDate.isEmpty() || groupBy == null || groupBy.isEmpty() || callType == null || callType.isEmpty()) {
            LOGGER.error("API was called with the following parameters: startDate {}, endDate {}, groupBy {}, callType {}", startDate, endDate, groupBy, callType);
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        //Defines if calls need to be filter by INBOUND, OUTBOUND or return all.
        List<String> callTypeFilter = new ArrayList<>();

        callTypeFilter = utils.setCallTypeFilter(callType);
        if(callTypeFilter.contains("invalid")) {
            LOGGER.error("Invalid parameters are passed: startDate {}, endDate {}, groupBy {}, callType {}", startDate, endDate, groupBy, callType);
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        if (startDate.length() == 10) {
            startDate = startDate + " 00:00";
        }
        if (endDate.length() == 10) {
            endDate = endDate + " 23:59";
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime start = LocalDateTime.parse(startDate, formatter);
        LocalDateTime end = LocalDateTime.parse(endDate, formatter);

        List<PotentialLeads> listOfLead = null;
        try {
            if (groupBy.equals("hourly")) {
                listOfLead = potentialLeadsRepository.getPotentialLeadsGroupByHour(awsAccountId, start, end, callTypeFilter);
                listOfLead = addDaysWithNoDataList(start, end, listOfLead, ChronoUnit.HOURS);
            } else if (groupBy.equals("daily")) {
                listOfLead = potentialLeadsRepository.getPotentialLeadsGroupByDay(awsAccountId, start, end, callTypeFilter);
                listOfLead = addDaysWithNoDataList(start, end, listOfLead, ChronoUnit.DAYS);
            }
            else {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
        } catch (Exception ex) {
            LOGGER.error("Unexpected error when pulling the PotentialLeads object");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        LOGGER.info("API has successfully return this object {}", listOfLead);
        return Response.ok(listOfLead).build();
    }


    /**
     *  This method will add a object with no data for all the dates between startDate and endDate that are not already on the list.
     *  This method will make sure that the length of the list returned will be equal to the number of days between startDate and endDate
     * @param startDate
     * @param endDate
     * @param list = list of items returned by API
     * @param inteval = Type ChronoUnit, it is the incremental amount (ChronoUnit.HOURS, ChronoUnit.DAYS)
     * @return a list of items that include also objects with no data.
     */
    private List<PotentialLeads> addDaysWithNoDataList(LocalDateTime startDate, LocalDateTime endDate, List<PotentialLeads> list, ChronoUnit inteval) {

        List<PotentialLeads> listOfLeadsIncludeNoDataObjects = new ArrayList<PotentialLeads>();
        Integer counter = 0;

        if(list.isEmpty() || list == null) {
            for (LocalDateTime date = startDate; date.isBefore(endDate); date = date.plus(1, inteval)) {
                PotentialLeads emptyObject = new PotentialLeads(date, 0L,0L,0L,0L,0L,0L,0L,0L,0L,0L,0L,0L,0L);
                listOfLeadsIncludeNoDataObjects.add(emptyObject);
            }
        }
        else {
            for (LocalDateTime date = startDate; date.isBefore(endDate); date = date.plus(1, inteval)) {

                if (counter >= list.size()) {
                    PotentialLeads emptyObject = new PotentialLeads(date, 0L,0L,0L,0L,0L,0L,0L,0L,0L,0L,0L,0L,0L);
                    listOfLeadsIncludeNoDataObjects.add(emptyObject);
                } else {
                    PotentialLeads object = list.get(counter);
                    if(object.getInterval().equals(date)) {
                        listOfLeadsIncludeNoDataObjects.add(object);
                        counter++;
                    }
                    else {
                        PotentialLeads emptyObject = new PotentialLeads(date, 0L,0L,0L,0L,0L,0L,0L,0L,0L,0L,0L,0L,0L);
                        listOfLeadsIncludeNoDataObjects.add(emptyObject);
                    }
                }



            }
        }

        return listOfLeadsIncludeNoDataObjects;
    }


}
