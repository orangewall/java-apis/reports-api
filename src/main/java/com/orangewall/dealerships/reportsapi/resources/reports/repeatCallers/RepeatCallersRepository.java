package com.orangewall.dealerships.reportsapi.resources.reports.repeatCallers;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Arjan on 3/05/2018.
 */
public interface RepeatCallersRepository extends Repository<RepeatCallers, Integer> {


    @Query(value = "SELECT number_of_times_called, COUNT(customer_endpoint_address) as number_of_callers\n" +
            "FROM (SELECT c.customer_endpoint_address, COUNT(c.id) as number_of_times_called\n" +
            "\t\tFROM public.call_records as c\n" +
            "    \tWHERE c.aws_account_id = :awsAccountId\n" +
            "    \tAND c.connected_to_system_timestamp>=:startDate\n" +
            "    \tAND c.connected_to_system_timestamp<=:endDate\n" +
            "      AND c.initiation_method in (:callTypeFilter)" +
            "\t\tGROUP by c.customer_endpoint_address\n" +
            "     ) as existing_callers_table\n" +
            "GROUP by number_of_times_called\n" +
            "order by number_of_times_called\n" +
            "\n", nativeQuery = true)
    List<RepeatCallers> getRepeatCallers(@Param("awsAccountId") String awsAccountId, @Param("startDate") LocalDateTime startDate, @Param("endDate") LocalDateTime endDate, @Param("callTypeFilter") List<String> callTypeFilter);


//    @Query(value = "SELECT number_of_times_called, COUNT(customer_endpoint_address) as number_of_callers\n" +
//            "FROM (SELECT c.customer_endpoint_address, COUNT(c.id) as number_of_times_called\n" +
//            "\t\tFROM public.call_records as c\n" +
//            "    \tWHERE c.aws_account_id = :awsAccountId\n" +
//            "    \tAND c.connected_to_system_timestamp>=:startDate\n" +
//            "    \tAND c.connected_to_system_timestamp<=:endDate\n" +
//            "      AND c.initiation_method in (:callTypeFilter)" +
//            "\t\tGROUP by c.customer_endpoint_address\n" +
//            "     ) as existing_callers_table\n" +
//            "GROUP by number_of_times_called\n" +
//            "order by number_of_times_called\n" +
//            "\n", nativeQuery = true)
//    List<RepeatCallers> getCallVolumeGroupByDay(@Param("awsAccountId") String awsAccountId, @Param("startDate") LocalDateTime startDate, @Param("endDate") LocalDateTime endDate, @Param("callTypeFilter") List<String> callTypeFilter);

}
