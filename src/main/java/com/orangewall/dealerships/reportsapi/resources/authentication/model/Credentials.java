package com.orangewall.dealerships.reportsapi.resources.authentication.model;

/**
 * Created by kbw780 on 6/6/17.
 */
public class Credentials {
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
