package com.orangewall.dealerships.reportsapi.resources.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.security.PermitAll;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

/**
 * Created by kbw780 on 6/6/17.
 */
@Path("log")
public class LoggingResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingResource.class);

    @POST
    @PermitAll
    public void log(LogMessage message) {
        if (message == null || message.getType() == null || message.getType().equals("") || message.getMessage() == null || message.getMessage().equals("")) {
            LOGGER.error("Failed to log the message since message was malformed! Message: '{}'", message);
        }
        if (message.getType().equals("info")) {
            LOGGER.info(message.getMessage());
        } else if (message.getType().equals("error")) {
            LOGGER.error(message.getMessage());
        } else if (message.getType().equals("warn")) {
            LOGGER.warn(message.getMessage());
        }
    }
}
