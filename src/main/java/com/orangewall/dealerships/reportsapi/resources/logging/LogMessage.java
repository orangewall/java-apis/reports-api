package com.orangewall.dealerships.reportsapi.resources.logging;

/**
 * Created by kbw780 on 6/6/17.
 */
public class LogMessage {
    private String type;
    private String message;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
