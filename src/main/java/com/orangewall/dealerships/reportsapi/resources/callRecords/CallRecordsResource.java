package com.orangewall.dealerships.reportsapi.resources.callRecords;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


/**
 * Created by Arjan on 3/05/2018.
 */

@Path("reports")
@Component
public class CallRecordsResource {
    @Autowired
    CallRecordsRepository callRecordsRepository;

    @Context
    UriInfo uriInfo;

    @Context
    Request request;

    @Context
    HttpServletRequest requestContext;

    private String awsAccountId="12";


    @GET
    @PermitAll
    @Path("employeeEngagement")
//    @QueryParam("{startDate},{endDate}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getEmployeeEngagement(@QueryParam("startDate") String startDate, @QueryParam("endDate") String endDate)  {

//        String test = "2018-03-19";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime start = LocalDateTime.parse(startDate, formatter);
        LocalDateTime end = LocalDateTime.parse(endDate, formatter);
        List<EmployeeEngagement> listOfCalls;
        try {

            listOfCalls = callRecordsRepository.getEmployeeEngagement(awsAccountId, start, end);
            return Response.ok(listOfCalls).build();
        }
        catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }


    private boolean validateRequiredFieldsCallRecord(CallRecords callRecords) {
        if(callRecords.getAwsAccountId() == null || callRecords.getAwsAccountId().isEmpty()) {
            return false;
        }
        else
            return true;
    }

}
