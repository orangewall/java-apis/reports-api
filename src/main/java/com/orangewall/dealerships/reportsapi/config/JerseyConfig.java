package com.orangewall.dealerships.reportsapi.config;

import com.orangewall.dealerships.reportsapi.common.CORSFilter;
import com.orangewall.dealerships.reportsapi.common.jaxrsFormats.csv.CSVMessageBodyWritter;
import com.orangewall.dealerships.reportsapi.resources.authentication.AuthenticationResource;
import com.orangewall.dealerships.reportsapi.resources.callRecords.CallRecordsResource;
import com.orangewall.dealerships.reportsapi.resources.logging.LoggingResource;
import com.orangewall.dealerships.reportsapi.resources.reports.callVolume.CallVolumeResource;
import com.orangewall.dealerships.reportsapi.resources.reports.potentialLeads.PotentialLeadsResource;
import com.orangewall.dealerships.reportsapi.resources.reports.repeatCallers.RepeatCallersResource;
import com.orangewall.dealerships.reportsapi.resources.user.UserResource;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

/**
 * Created by Arjan on 11/26/2017.
 */
@Component
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig(){
//        packages("com.orangewall.dealerships.resources"); //Jersey has a problem if you register packages. Register individual classes instead
        register(CSVMessageBodyWritter.class);
        register(CallRecordsResource.class);
        register(PotentialLeadsResource.class);
        register(CallVolumeResource.class);
        register(RepeatCallersResource.class);
        register(AuthenticationResource.class);
        register(LoggingResource.class);
        register(UserResource.class);
        register(CORSFilter.class);
//        register(AuthenticationFilter.class);
    }
}
