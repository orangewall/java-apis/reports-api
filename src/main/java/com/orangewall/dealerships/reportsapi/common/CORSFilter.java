package com.orangewall.dealerships.reportsapi.common;

/**
 * Created by kbw780 on 6/6/17.
 */

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Component
@Provider
public class CORSFilter implements ContainerResponseFilter {

    @Value("${cors.allow-origin:*}")
    private String allowOrigin;

    @Value("${cors.allow-headers:origin, content-type, accept, authorization}")
    private String allowHeaders;

    @Value("${cors.allow-credentials:true}")
    private String allowCredentials;

    @Value("${cors.allow-methods:GET, POST, PUT, DELETE, OPTIONS, HEAD}")
    private String allowMethods;

    @Value("${cors.max-age:1209600}")
    private String maxAge;

    public void filter(final ContainerRequestContext requestContext,
                       final ContainerResponseContext cres) throws IOException {
        cres.getHeaders().add("Access-Control-Allow-Origin", allowOrigin);
        cres.getHeaders().add("Access-Control-Allow-Headers", allowHeaders);
        cres.getHeaders().add("Access-Control-Allow-Credentials", allowCredentials);
        cres.getHeaders().add("Access-Control-Allow-Methods", allowMethods);
        cres.getHeaders().add("Access-Control-Max-Age", maxAge);
    }
}