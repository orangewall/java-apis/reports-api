package com.orangewall.dealerships.reportsapi.common.authentication;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Priority;
import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * This filter verify the access permissions for a user
 * based on username and passowrd provided in request
 * */
@Provider
@Priority(Priorities.AUTHENTICATION)
@Component
public class AuthenticationFilter implements javax.ws.rs.container.ContainerRequestFilter {

    @Context
    private ResourceInfo resourceInfo;

    @Value("${authentication.header-property:Authorization}")
    private String authorizationHeader;

    @Value("${authentication.scheme:Bearer}")
    private String authenticationScheme;

    @Value("${authentication.url}")
    private String authenticationUrl;


    @Override
    public void filter(ContainerRequestContext requestContext) {
        if (requestContext.getRequest().getMethod().equals("OPTIONS")) {
            return;
        }
        Method method = resourceInfo.getResourceMethod();
        //Access allowed for all
        if (!method.isAnnotationPresent(PermitAll.class)) {
            //Access denied for all
            if (method.isAnnotationPresent(DenyAll.class)) {
                Response accessForbiden = Response.status(Response.Status.FORBIDDEN).entity("Access blocked for all users !!").build();
                requestContext.abortWith(accessForbiden);
                return;
            }

            //Get request headers
            final MultivaluedMap<String, String> headers = requestContext.getHeaders();

            //Fetch authorization header
            final List<String> authorization = headers.get(authorizationHeader);

            //If no authorization information present; block access
            if (authorization == null || authorization.isEmpty()) {
                Response accessDenied = Response.status(Response.Status.UNAUTHORIZED).entity("You cannot access this resource").build();
                requestContext.abortWith(accessDenied);
                return;
            }

            //Get encoded token
            final String token = authorization.get(0).replaceFirst(authenticationScheme + " ", "");

            //Decode token
//            String token = new String(Base64.getDecoder().decode(encodedUserPassword.getBytes()));


            //Verify user access
            if (method.isAnnotationPresent(RolesAllowed.class)) {
                RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
                Set<String> rolesSet = new HashSet<String>(Arrays.asList(rolesAnnotation.value()));

                //Is user valid?
                if (!isUserAllowed(token, rolesSet)) {
                    Response accessDenied = Response.status(Response.Status.UNAUTHORIZED).entity("You cannot access this resource").build();
                    requestContext.abortWith(accessDenied);
                    return;
                }
            }
        }
    }

    private boolean isUserAllowed(String token, Set<String> rolesSet) {
        if (token == null || token.isEmpty()) {
            return false;
        } else {
            if(rolesSet.contains("*")){
                return true;
            }

            RestTemplate restTemplate = new RestTemplate();
            String role = restTemplate.postForObject(this.authenticationUrl, token, String.class, new Object[0]);
            boolean isAllowed = false;
            if (role != null && rolesSet.contains(role)) {
                isAllowed = true;
            }

            return isAllowed;
        }
    }

}