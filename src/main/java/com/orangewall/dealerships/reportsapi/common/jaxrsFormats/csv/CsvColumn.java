package com.orangewall.dealerships.reportsapi.common.jaxrsFormats.csv;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CsvColumn {
    String name();
    boolean exclude() default false;
    int order() default -1;
}
