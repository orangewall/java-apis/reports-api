package com.orangewall.dealerships.reportsapi.common.jaxrsFormats.csv;


import org.springframework.util.StringUtils;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

//check this example for more complex: http://www.javaprocess.com/2015/08/a-simple-csv-messagebodywriter-for-jax.html
@Provider
@Produces("text/csv")
public class CSVMessageBodyWritter implements MessageBodyWriter {

    final static String separator = ",";
    final static String delimiter = "\n";

    @Override
    public boolean isWriteable(Class type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        boolean ret = List.class.isAssignableFrom(type);
        return ret;
    }


    @Override
    public void writeTo(Object o, Class aClass, Type type, Annotation[] annotations, MediaType mediaType, MultivaluedMap multivaluedMap, OutputStream outputStream) throws IOException, WebApplicationException {
        List data = (List) o;
        if (data != null && data.size() > 0) {
            Object obj = data.get(0);

            SortedMap<Integer, CsvFieldInfo> fieldsMap = getFieldsMap(obj.getClass());

            PrintWriter writer = new PrintWriter(outputStream);

            writer.write(createHeader(fieldsMap));
            for (Object el : data)
            {
                writer.write(createCsvLine(fieldsMap, el));
            }
            writer.flush();
            writer.close();
        }
    }

    private String createHeader(SortedMap<Integer, CsvFieldInfo> fieldsMap) {
        StringBuilder header = new StringBuilder("");
        for (CsvFieldInfo fieldInfo : fieldsMap.values()) {
            if (!header.toString().equals("")) {
                header.append(separator);
            }
            header.append(fieldInfo.ColumnName);
        }
        header.append(delimiter);
        return header.toString();
    }

    private String createCsvLine(SortedMap<Integer, CsvFieldInfo> fieldsMap, Object obj) {
        StringBuilder header = new StringBuilder("");
        for (CsvFieldInfo fieldInfo : fieldsMap.values()) {
            if (!header.toString().equals("")) {
                header.append(separator);
            }
            try {
                header.append(fieldInfo.getField().get(obj));
            } catch (IllegalAccessException ex) {
                return "";
            }
        }
        header.append(delimiter);
        return header.toString();
    }

    private SortedMap<Integer, CsvFieldInfo> getFieldsMap(Class aClass) {
        Field[] fields = aClass.getDeclaredFields();
        SortedMap<Integer, CsvFieldInfo> fieldMap = new TreeMap<>();
        int order = 10000; //Start from a high number so defined order values will come first
        for (Field field : fields) {
            CsvColumn annotation = field.getDeclaredAnnotation(CsvColumn.class);
            CsvFieldInfo fieldInfo = new CsvFieldInfo();
            fieldInfo.setOrder(order);
            if (annotation != null) {
                if (annotation.exclude()) {
                    break; //if it is exclude, dont add it to csv
                }
                fieldInfo.setColumnName(annotation.name());
                if (annotation.order() != -1) {
                    fieldInfo.setOrder(annotation.order());
                }
            } else {
                fieldInfo.setColumnName(getColumnName(field));
                fieldInfo.setOrder(order);
            }
            fieldInfo.setField(field);
            field.setAccessible(true);
            fieldMap.put(fieldInfo.getOrder(), fieldInfo);
            order++;
        }
        return fieldMap;
    }

    private String getColumnName(Field field) {
        String fieldName = field.getName();
        return splitCamelCase(fieldName);
    }

    static String splitCamelCase(String s) {

        return StringUtils.capitalize(s.replaceAll(
                String.format("%s|%s|%s",
                        "(?<=[A-Z])(?=[A-Z][a-z])",
                        "(?<=[^A-Z])(?=[A-Z])",
                        "(?<=[A-Za-z])(?=[^A-Za-z])"
                ),
                " "
        ));
    }


    @Override
    public long getSize(Object o, Class type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return 0;
    }

    private class CsvFieldInfo {
        private String ColumnName;
        private Field field;
        private Integer order;

        public String getColumnName() {
            return ColumnName;
        }

        public void setColumnName(String columnName) {
            ColumnName = columnName;
        }

        public Field getField() {
            return field;
        }

        public void setField(Field field) {
            this.field = field;
        }

        public Integer getOrder() {
            return order;
        }

        public void setOrder(Integer order) {
            this.order = order;
        }
    }
}