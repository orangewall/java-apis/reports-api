package com.orangewall.dealerships.reportsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import java.util.ArrayList;

/**
 * Created by Arjan on 11/26/2017.
 */
@SpringBootApplication
@ComponentScan("com.orangewall")
@ImportResource("classpath:springConfig.xml")
public class OrangeWallApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrangeWallApplication.class, args);
    }
}
